-- 1 Selecione todos os tenistas cadastrados apresentando todos campos da tabela
SELECT * FROM public.tenista
ORDER BY id_pessoa ASC;

-- 2 Selecione os tenistas que participaram de algum torneio
SELECT * FROM public.Tenista T
	INNER JOIN public.Inscricao I
		ON T.id_Pessoa = I.Tenista_id_Pessoa;

-- 3 Selecione tenistas de acordo com um nível técnico específico
SELECT * FROM public.Tenista where NivelTecnico_nivel = 1;

-- 4 Selecione os tenistas de um clube
SELECT Ten.id_Pessoa, Ten.nomePessoa, L.nomeClube FROM public.Tenista Ten
	INNER JOIN public.Inscricao I
		ON Ten.id_Pessoa = I.Tenista_id_Pessoa
	INNER JOIN public.Torneio Tor
        	ON Tor.idTorneio = I.Torneio_idTorneio
	inner join public.LocalJogo L
		ON L.cep = Tor.LocalJogo_cep
WHERE Tor.LocalJogo_cep = 40411010;

-- 5 Selecione os tenistas de um torneio específico
SELECT * FROM public.Tenista T
    INNER JOIN public.Inscricao I
        ON T.id_Pessoa = I.Tenista_id_Pessoa
WHERE I.Torneio_idTorneio = 1;

-- 6 Selecione os torneios de um ano qualquer
select * from public.Torneio where EXTRACT(YEAR FROM dt_Torneio) = 2020;

-- 7 Selecione os torneios anteriores ao ano atual
select * from public.Torneio where EXTRACT(YEAR FROM dt_Torneio) < EXTRACT(year from(now()));

-- 8 Selecione os jogos de um tenista em um torneio
SELECT
	J.idJogo AS idJogo,
	(SELECT tipoFase from public.Fase where idFase = J.Fase_idFase) AS Fase_jogo,
	Ten.nomePessoa AS Nome_Tenista,
	(SELECT nomePessoa from public.Tenista where id_Pessoa = J.idTenista2) AS Nome_Adversario
FROM public.Jogo J
	INNER JOIN public.Tenista Ten
		ON Ten.id_Pessoa = J.idTenista1
	INNER JOIN public.Inscricao I
		ON I.Tenista_id_Pessoa = Ten.id_Pessoa
	INNER JOIN public.Torneio Tor
		ON Tor.idTorneio = I.Torneio_idTorneio
	WHERE Ten.Id_Pessoa = 1 and Tor.idTorneio = 1
UNION
SELECT
	J.idJogo AS idJogo,
	(SELECT tipoFase from public.Fase where idFase = J.Fase_idFase) AS Fase_jogo,
	Ten.nomePessoa AS Nome_Tenista,
	(SELECT nomePessoa from public.Tenista where id_Pessoa = J.idTenista1) AS Nome_Adversario
FROM public.Jogo J
	INNER JOIN public.Tenista Ten
		ON Ten.id_Pessoa = J.idTenista2
	INNER JOIN public.Inscricao I
		ON I.Tenista_id_Pessoa = Ten.id_Pessoa
	INNER JOIN public.Torneio Tor
		ON Tor.idTorneio = I.Torneio_idTorneio
	WHERE Ten.Id_Pessoa = 1 and Tor.idTorneio = 1;

-- 9 Selecione os jogos de um torneio
SELECT
	J.idJogo AS idJogo,
	(SELECT tipoFase from public.Fase where idFase = J.Fase_idFase) AS Fase_jogo,
	Ten.nomePessoa AS Nome_Tenista,
	(SELECT nomePessoa from public.Tenista where id_Pessoa = J.idTenista2) AS Nome_Adversario
FROM public.Jogo J
	INNER JOIN public.Tenista Ten
		ON Ten.id_Pessoa = J.idTenista1
	INNER JOIN public.Inscricao I
		ON I.Tenista_id_Pessoa = Ten.id_Pessoa
	INNER JOIN public.Torneio Tor
		ON Tor.idTorneio = I.Torneio_idTorneio
	WHERE Tor.idTorneio = 1;

-- 10 Selecione os jogos de um torneio para um determinado nível técnico
SELECT
	J.idJogo AS idJogo,
	(SELECT tipoFase from public.Fase where idFase = J.Fase_idFase) AS Fase_jogo,
	Ten.nomePessoa AS Nome_Tenista,
	(SELECT nomePessoa from public.Tenista where id_Pessoa = J.idTenista2) AS Nome_Adversario
FROM public.Jogo J
	INNER JOIN public.Tenista Ten
		ON Ten.id_Pessoa = J.idTenista1
	INNER JOIN public.Inscricao I
		ON I.Tenista_id_Pessoa = Ten.id_Pessoa
	INNER JOIN public.Torneio Tor
		ON Tor.idTorneio = I.Torneio_idTorneio
	WHERE Ten.niveltecnico_nivel = 7 and Tor.idTorneio = 1;