INSERT INTO public.LocalJogo(cep, cidade, estado, pais, nomeClube)
VALUES(40411010, 'Salvador', 'BA', 'Brazil', 'Quadras de tênis da Boca do Rio');


INSERT INTO public.NivelTecnico(nivel, caracteristica)
VALUES(7, 'Profissional');


INSERT INTO public.Fase(tipoFase)
VALUES('Oitavas de Final');


INSERT INTO public.Torneio(LocalJogo_cep, dt_Torneio)
VALUES(40411010, '2020-12-13');


INSERT INTO public.Tenista (cpfPessoa, nomePessoa, dataNasc, sexo, NivelTecnico_nivel, altura, peso)
VALUES (88587368001, 'Fernando Meligeni', '1971-04-12', 'M', 7, 1.80, 64);


INSERT INTO public.Tenista (cpfPessoa, nomePessoa, dataNasc, sexo, NivelTecnico_nivel, altura, peso)
VALUES (61769691073, 'Gustavo Kuerten', '1976-09-10', 'M', 7, 1.91, 83);


INSERT INTO public.Professor (cpfPessoa, nomePessoa, dataNasc, sexo, Tenista_id_Pessoa, matriculaProf)
VALUES (52790281009, 'Agnaldo de Jesus Nascimento', '1963-03-27', 'M', 1, 49703);


INSERT INTO public.Professor (cpfPessoa, nomePessoa, dataNasc, sexo, Tenista_id_Pessoa, matriculaProf)
VALUES (38308671403, 'César Miguel Gustavo de Paula', '1995-08-10', 'M', 2, 14962);


INSERT INTO public.Inscricao (Torneio_idTorneio, Tenista_id_Pessoa)
VALUES (1, 1);


INSERT INTO public.Inscricao (Torneio_idTorneio, Tenista_id_Pessoa)
VALUES (1, 2);


INSERT INTO public.Jogo (Fase_idFase, idTenista1, idTenista2, fase)
VALUES (1, 1, 2, 'A');