CREATE TABLE public.LocalJogo (
  cep INTEGER   NOT NULL ,
  cidade CHAR(50)    ,
  estado CHAR(30)    ,
  pais CHAR(45)    ,
  nomeClube CHAR(255)      ,
PRIMARY KEY(cep))
TABLESPACE pg_default;

ALTER TABLE public.LocalJogo
    OWNER to postgres;


CREATE TABLE public.NivelTecnico (
  nivel INTEGER   NOT NULL ,
  caracteristica CHAR(255)      ,
PRIMARY KEY(nivel))
TABLESPACE pg_default;

ALTER TABLE public.NivelTecnico
    OWNER to postgres;

CREATE TABLE public.Pessoa (
  id_Pessoa SERIAL,
  cpfPessoa BIGINT   NOT NULL ,
  nomePessoa VARCHAR (100),
  dataNasc DATE    ,
  sexo CHAR      ,
PRIMARY KEY(id_Pessoa))
TABLESPACE pg_default;

ALTER TABLE public.Pessoa
    OWNER to postgres;

CREATE TABLE public.Fase (
  idFase SERIAL ,
  tipoFase VARCHAR(40)      ,
PRIMARY KEY(idFase))
TABLESPACE pg_default;

ALTER TABLE public.Fase
    OWNER to postgres;

CREATE TABLE public.Torneio (
  idTorneio SERIAL,
  LocalJogo_cep INTEGER   NOT NULL ,
  dt_Torneio DATE,
PRIMARY KEY(idTorneio),
  FOREIGN KEY(LocalJogo_cep)
    REFERENCES LocalJogo(cep))
TABLESPACE pg_default;

ALTER TABLE public.Torneio
    OWNER to postgres;

CREATE TABLE public.Tenista (
  --id_Pessoa SERIAL, (Herdado da tabela public.Pessoa)
  --cpfPessoa INTEGER   NOT NULL , (Herdado da tabela public.Pessoa)
  --nomePessoa INTEGER    , (Herdado da tabela public.Pessoa)
  --dataNasc DATE    , (Herdado da tabela public.Pessoa)
  --sexo CHAR      , (Herdado da tabela public.Pessoa)
  NivelTecnico_nivel INTEGER   NOT NULL ,
  altura FLOAT    ,
  peso FLOAT      ,
  CONSTRAINT tenista_pkey PRIMARY KEY (id_Pessoa),
  FOREIGN KEY(NivelTecnico_nivel)
    REFERENCES NivelTecnico(nivel))
INHERITS(public.Pessoa)
TABLESPACE pg_default;

ALTER TABLE public.Tenista
    OWNER to postgres;

--INSERT INTO public.Tenista (cpfPessoa, nomePessoa, dataNas, sexo, NivelTecnico_nivel, altura, peso) VALUES ();

SELECT * from public.Tenista;

CREATE TABLE public.Professor (
  --id_Pessoa SERIAL, (Herdado da tabela public.Pessoa)
  --cpfPessoa INTEGER   NOT NULL , (Herdado da tabela public.Pessoa)
  --nomePessoa INTEGER    , (Herdado da tabela public.Pessoa)
  --dataNasc DATE    , (Herdado da tabela public.Pessoa)
  --sexo CHAR      , (Herdado da tabela public.Pessoa)
  Tenista_id_Pessoa INTEGER   NOT NULL ,
  matriculaProf INTEGER   NOT NULL   ,
  FOREIGN KEY(Tenista_id_Pessoa)
    REFERENCES Tenista(id_Pessoa))
  INHERITS(public.Pessoa)
TABLESPACE pg_default;

ALTER TABLE public.Professor
    OWNER to postgres;

--INSERT INTO public.Professor (cpfPessoa, nomePessoa, dataNas, sexo, Tenista_id_Pessoa, matriculaProf) VALUES ();

CREATE TABLE public.Inscricao (
  idInscricao SERIAL,
  Torneio_idTorneio INTEGER   NOT NULL ,
  Tenista_id_Pessoa INTEGER   NOT NULL   ,
PRIMARY KEY(idInscricao),
  FOREIGN KEY(Tenista_id_Pessoa)
    REFERENCES Tenista(id_Pessoa),
  FOREIGN KEY(Torneio_idTorneio)
    REFERENCES Torneio(idTorneio))
TABLESPACE pg_default;

ALTER TABLE public.Inscricao
    OWNER to postgres;

CREATE TABLE public.Jogo (
  idJogo SERIAL,
  Fase_idFase INTEGER   NOT NULL ,
  idTenista1 INTEGER   NOT NULL ,
  idTenista2 INTEGER   NOT NULL ,
  fase CHAR      ,
PRIMARY KEY(idJogo),
  FOREIGN KEY(idTenista1)
    REFERENCES Tenista(id_Pessoa),
  FOREIGN KEY(idTenista2)
    REFERENCES Tenista(id_Pessoa),
  FOREIGN KEY(Fase_idFase)
    REFERENCES Fase(idFase))
TABLESPACE pg_default;

ALTER TABLE public.Jogo
    OWNER to postgres;