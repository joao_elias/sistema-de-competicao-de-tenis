PGDMP     6    )                x            sistema-tenis-ifpb    13.1    13.1 <    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    16544    sistema-tenis-ifpb    DATABASE     t   CREATE DATABASE "sistema-tenis-ifpb" WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Portuguese_Brazil.1252';
 $   DROP DATABASE "sistema-tenis-ifpb";
                postgres    false            �            1259    16565    fase    TABLE     ^   CREATE TABLE public.fase (
    idfase integer NOT NULL,
    tipofase character varying(40)
);
    DROP TABLE public.fase;
       public         heap    postgres    false            �            1259    16563    fase_idfase_seq    SEQUENCE     �   CREATE SEQUENCE public.fase_idfase_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.fase_idfase_seq;
       public          postgres    false    205                        0    0    fase_idfase_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.fase_idfase_seq OWNED BY public.fase.idfase;
          public          postgres    false    204            �            1259    16609 	   inscricao    TABLE     �   CREATE TABLE public.inscricao (
    idinscricao integer NOT NULL,
    torneio_idtorneio integer NOT NULL,
    tenista_id_pessoa integer NOT NULL
);
    DROP TABLE public.inscricao;
       public         heap    postgres    false            �            1259    16607    inscricao_idinscricao_seq    SEQUENCE     �   CREATE SEQUENCE public.inscricao_idinscricao_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.inscricao_idinscricao_seq;
       public          postgres    false    211                       0    0    inscricao_idinscricao_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.inscricao_idinscricao_seq OWNED BY public.inscricao.idinscricao;
          public          postgres    false    210            �            1259    16627    jogo    TABLE     �   CREATE TABLE public.jogo (
    idjogo integer NOT NULL,
    fase_idfase integer NOT NULL,
    idtenista1 integer NOT NULL,
    idtenista2 integer NOT NULL,
    fase character(1)
);
    DROP TABLE public.jogo;
       public         heap    postgres    false            �            1259    16625    jogo_idjogo_seq    SEQUENCE     �   CREATE SEQUENCE public.jogo_idjogo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.jogo_idjogo_seq;
       public          postgres    false    213                       0    0    jogo_idjogo_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.jogo_idjogo_seq OWNED BY public.jogo.idjogo;
          public          postgres    false    212            �            1259    16545 	   localjogo    TABLE     �   CREATE TABLE public.localjogo (
    cep integer NOT NULL,
    cidade character(50),
    estado character(30),
    pais character(45),
    nomeclube character(255)
);
    DROP TABLE public.localjogo;
       public         heap    postgres    false            �            1259    16550    niveltecnico    TABLE     d   CREATE TABLE public.niveltecnico (
    nivel integer NOT NULL,
    caracteristica character(255)
);
     DROP TABLE public.niveltecnico;
       public         heap    postgres    false            �            1259    16557    pessoa    TABLE     �   CREATE TABLE public.pessoa (
    id_pessoa integer NOT NULL,
    cpfpessoa bigint NOT NULL,
    nomepessoa character varying(100),
    datanasc date,
    sexo character(1)
);
    DROP TABLE public.pessoa;
       public         heap    postgres    false            �            1259    16555    pessoa_id_pessoa_seq    SEQUENCE     �   CREATE SEQUENCE public.pessoa_id_pessoa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.pessoa_id_pessoa_seq;
       public          postgres    false    203                       0    0    pessoa_id_pessoa_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.pessoa_id_pessoa_seq OWNED BY public.pessoa.id_pessoa;
          public          postgres    false    202            �            1259    16598 	   professor    TABLE     �   CREATE TABLE public.professor (
    tenista_id_pessoa integer NOT NULL,
    matriculaprof integer NOT NULL
)
INHERITS (public.pessoa);
    DROP TABLE public.professor;
       public         heap    postgres    false    203            �            1259    16587    tenista    TABLE     �   CREATE TABLE public.tenista (
    niveltecnico_nivel integer NOT NULL,
    altura double precision,
    peso double precision
)
INHERITS (public.pessoa);
    DROP TABLE public.tenista;
       public         heap    postgres    false    203            �            1259    16573    torneio    TABLE     y   CREATE TABLE public.torneio (
    idtorneio integer NOT NULL,
    localjogo_cep integer NOT NULL,
    dt_torneio date
);
    DROP TABLE public.torneio;
       public         heap    postgres    false            �            1259    16571    torneio_idtorneio_seq    SEQUENCE     �   CREATE SEQUENCE public.torneio_idtorneio_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.torneio_idtorneio_seq;
       public          postgres    false    207                       0    0    torneio_idtorneio_seq    SEQUENCE OWNED BY     O   ALTER SEQUENCE public.torneio_idtorneio_seq OWNED BY public.torneio.idtorneio;
          public          postgres    false    206            L           2604    16568    fase idfase    DEFAULT     j   ALTER TABLE ONLY public.fase ALTER COLUMN idfase SET DEFAULT nextval('public.fase_idfase_seq'::regclass);
 :   ALTER TABLE public.fase ALTER COLUMN idfase DROP DEFAULT;
       public          postgres    false    204    205    205            P           2604    16612    inscricao idinscricao    DEFAULT     ~   ALTER TABLE ONLY public.inscricao ALTER COLUMN idinscricao SET DEFAULT nextval('public.inscricao_idinscricao_seq'::regclass);
 D   ALTER TABLE public.inscricao ALTER COLUMN idinscricao DROP DEFAULT;
       public          postgres    false    210    211    211            Q           2604    16630    jogo idjogo    DEFAULT     j   ALTER TABLE ONLY public.jogo ALTER COLUMN idjogo SET DEFAULT nextval('public.jogo_idjogo_seq'::regclass);
 :   ALTER TABLE public.jogo ALTER COLUMN idjogo DROP DEFAULT;
       public          postgres    false    213    212    213            K           2604    16560    pessoa id_pessoa    DEFAULT     t   ALTER TABLE ONLY public.pessoa ALTER COLUMN id_pessoa SET DEFAULT nextval('public.pessoa_id_pessoa_seq'::regclass);
 ?   ALTER TABLE public.pessoa ALTER COLUMN id_pessoa DROP DEFAULT;
       public          postgres    false    202    203    203            O           2604    16601    professor id_pessoa    DEFAULT     w   ALTER TABLE ONLY public.professor ALTER COLUMN id_pessoa SET DEFAULT nextval('public.pessoa_id_pessoa_seq'::regclass);
 B   ALTER TABLE public.professor ALTER COLUMN id_pessoa DROP DEFAULT;
       public          postgres    false    202    209            N           2604    16590    tenista id_pessoa    DEFAULT     u   ALTER TABLE ONLY public.tenista ALTER COLUMN id_pessoa SET DEFAULT nextval('public.pessoa_id_pessoa_seq'::regclass);
 @   ALTER TABLE public.tenista ALTER COLUMN id_pessoa DROP DEFAULT;
       public          postgres    false    208    202            M           2604    16576    torneio idtorneio    DEFAULT     v   ALTER TABLE ONLY public.torneio ALTER COLUMN idtorneio SET DEFAULT nextval('public.torneio_idtorneio_seq'::regclass);
 @   ALTER TABLE public.torneio ALTER COLUMN idtorneio DROP DEFAULT;
       public          postgres    false    206    207    207            �          0    16565    fase 
   TABLE DATA           0   COPY public.fase (idfase, tipofase) FROM stdin;
    public          postgres    false    205   �D       �          0    16609 	   inscricao 
   TABLE DATA           V   COPY public.inscricao (idinscricao, torneio_idtorneio, tenista_id_pessoa) FROM stdin;
    public          postgres    false    211   �D       �          0    16627    jogo 
   TABLE DATA           Q   COPY public.jogo (idjogo, fase_idfase, idtenista1, idtenista2, fase) FROM stdin;
    public          postgres    false    213   E       �          0    16545 	   localjogo 
   TABLE DATA           I   COPY public.localjogo (cep, cidade, estado, pais, nomeclube) FROM stdin;
    public          postgres    false    200   +E       �          0    16550    niveltecnico 
   TABLE DATA           =   COPY public.niveltecnico (nivel, caracteristica) FROM stdin;
    public          postgres    false    201   �E       �          0    16557    pessoa 
   TABLE DATA           R   COPY public.pessoa (id_pessoa, cpfpessoa, nomepessoa, datanasc, sexo) FROM stdin;
    public          postgres    false    203   �E       �          0    16598 	   professor 
   TABLE DATA           w   COPY public.professor (id_pessoa, cpfpessoa, nomepessoa, datanasc, sexo, tenista_id_pessoa, matriculaprof) FROM stdin;
    public          postgres    false    209   �E       �          0    16587    tenista 
   TABLE DATA           u   COPY public.tenista (id_pessoa, cpfpessoa, nomepessoa, datanasc, sexo, niveltecnico_nivel, altura, peso) FROM stdin;
    public          postgres    false    208   kF       �          0    16573    torneio 
   TABLE DATA           G   COPY public.torneio (idtorneio, localjogo_cep, dt_torneio) FROM stdin;
    public          postgres    false    207   �F                  0    0    fase_idfase_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.fase_idfase_seq', 1, true);
          public          postgres    false    204                       0    0    inscricao_idinscricao_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.inscricao_idinscricao_seq', 2, true);
          public          postgres    false    210                       0    0    jogo_idjogo_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.jogo_idjogo_seq', 1, true);
          public          postgres    false    212                       0    0    pessoa_id_pessoa_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.pessoa_id_pessoa_seq', 5, true);
          public          postgres    false    202            	           0    0    torneio_idtorneio_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.torneio_idtorneio_seq', 1, true);
          public          postgres    false    206            Y           2606    16570    fase fase_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.fase
    ADD CONSTRAINT fase_pkey PRIMARY KEY (idfase);
 8   ALTER TABLE ONLY public.fase DROP CONSTRAINT fase_pkey;
       public            postgres    false    205            _           2606    16614    inscricao inscricao_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.inscricao
    ADD CONSTRAINT inscricao_pkey PRIMARY KEY (idinscricao);
 B   ALTER TABLE ONLY public.inscricao DROP CONSTRAINT inscricao_pkey;
       public            postgres    false    211            a           2606    16632    jogo jogo_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.jogo
    ADD CONSTRAINT jogo_pkey PRIMARY KEY (idjogo);
 8   ALTER TABLE ONLY public.jogo DROP CONSTRAINT jogo_pkey;
       public            postgres    false    213            S           2606    16549    localjogo localjogo_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.localjogo
    ADD CONSTRAINT localjogo_pkey PRIMARY KEY (cep);
 B   ALTER TABLE ONLY public.localjogo DROP CONSTRAINT localjogo_pkey;
       public            postgres    false    200            U           2606    16554    niveltecnico niveltecnico_pkey 
   CONSTRAINT     _   ALTER TABLE ONLY public.niveltecnico
    ADD CONSTRAINT niveltecnico_pkey PRIMARY KEY (nivel);
 H   ALTER TABLE ONLY public.niveltecnico DROP CONSTRAINT niveltecnico_pkey;
       public            postgres    false    201            W           2606    16562    pessoa pessoa_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.pessoa
    ADD CONSTRAINT pessoa_pkey PRIMARY KEY (id_pessoa);
 <   ALTER TABLE ONLY public.pessoa DROP CONSTRAINT pessoa_pkey;
       public            postgres    false    203            ]           2606    16592    tenista tenista_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.tenista
    ADD CONSTRAINT tenista_pkey PRIMARY KEY (id_pessoa);
 >   ALTER TABLE ONLY public.tenista DROP CONSTRAINT tenista_pkey;
       public            postgres    false    208            [           2606    16581    torneio torneio_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.torneio
    ADD CONSTRAINT torneio_pkey PRIMARY KEY (idtorneio);
 >   ALTER TABLE ONLY public.torneio DROP CONSTRAINT torneio_pkey;
       public            postgres    false    207            e           2606    16615 *   inscricao inscricao_tenista_id_pessoa_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inscricao
    ADD CONSTRAINT inscricao_tenista_id_pessoa_fkey FOREIGN KEY (tenista_id_pessoa) REFERENCES public.tenista(id_pessoa);
 T   ALTER TABLE ONLY public.inscricao DROP CONSTRAINT inscricao_tenista_id_pessoa_fkey;
       public          postgres    false    211    2909    208            f           2606    16620 *   inscricao inscricao_torneio_idtorneio_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.inscricao
    ADD CONSTRAINT inscricao_torneio_idtorneio_fkey FOREIGN KEY (torneio_idtorneio) REFERENCES public.torneio(idtorneio);
 T   ALTER TABLE ONLY public.inscricao DROP CONSTRAINT inscricao_torneio_idtorneio_fkey;
       public          postgres    false    211    207    2907            i           2606    16643    jogo jogo_fase_idfase_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.jogo
    ADD CONSTRAINT jogo_fase_idfase_fkey FOREIGN KEY (fase_idfase) REFERENCES public.fase(idfase);
 D   ALTER TABLE ONLY public.jogo DROP CONSTRAINT jogo_fase_idfase_fkey;
       public          postgres    false    205    2905    213            g           2606    16633    jogo jogo_idtenista1_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.jogo
    ADD CONSTRAINT jogo_idtenista1_fkey FOREIGN KEY (idtenista1) REFERENCES public.tenista(id_pessoa);
 C   ALTER TABLE ONLY public.jogo DROP CONSTRAINT jogo_idtenista1_fkey;
       public          postgres    false    2909    213    208            h           2606    16638    jogo jogo_idtenista2_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.jogo
    ADD CONSTRAINT jogo_idtenista2_fkey FOREIGN KEY (idtenista2) REFERENCES public.tenista(id_pessoa);
 C   ALTER TABLE ONLY public.jogo DROP CONSTRAINT jogo_idtenista2_fkey;
       public          postgres    false    213    208    2909            d           2606    16602 *   professor professor_tenista_id_pessoa_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.professor
    ADD CONSTRAINT professor_tenista_id_pessoa_fkey FOREIGN KEY (tenista_id_pessoa) REFERENCES public.tenista(id_pessoa);
 T   ALTER TABLE ONLY public.professor DROP CONSTRAINT professor_tenista_id_pessoa_fkey;
       public          postgres    false    209    208    2909            c           2606    16593 '   tenista tenista_niveltecnico_nivel_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.tenista
    ADD CONSTRAINT tenista_niveltecnico_nivel_fkey FOREIGN KEY (niveltecnico_nivel) REFERENCES public.niveltecnico(nivel);
 Q   ALTER TABLE ONLY public.tenista DROP CONSTRAINT tenista_niveltecnico_nivel_fkey;
       public          postgres    false    2901    208    201            b           2606    16582 "   torneio torneio_localjogo_cep_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.torneio
    ADD CONSTRAINT torneio_localjogo_cep_fkey FOREIGN KEY (localjogo_cep) REFERENCES public.localjogo(cep);
 L   ALTER TABLE ONLY public.torneio DROP CONSTRAINT torneio_localjogo_cep_fkey;
       public          postgres    false    207    200    2899            �       x�3���,I,K,VHIUp��K������ ]�      �      x�3�4�4�2�F\1z\\\ 	      �      x�3�4B#NG�=... ��      �   R   x�31014404�N�)KL�/R p:9�.J���!ְ��Ĕ��b��T��ë�2��D���D��|���|��54W� ��=�      �      x�3�(�O�,.���K�Qy�+F��� m	$L      �      x������ � �      �   �   x��K
�0 ����@d&�Y�A�x 7��[0�w�^��;��lb����"u\�X�����U�c~�e[�8:���xN�T ��ȣ����䥇y��so����Mz�}�`0�}�@��U��R�i�"}      �   k   x�-�M
�@�ur
/0�ϱ���]oЍ�P�2�՞����~q��"�)�������Z��VB���d��IGHa�t=���ٺۙ���w�H���+�{f����      �   #   x�3�41014404�4202�54�54����� @�f     